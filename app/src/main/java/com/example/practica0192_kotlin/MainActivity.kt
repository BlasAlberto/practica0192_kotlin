package com.example.practica0192_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    // Declarar componentes del Layout
    private lateinit var btnSaludar : Button;
    private lateinit var btnLimpiar : Button;
    private lateinit var btnCerrar : Button;
    private lateinit var txtNombre : EditText;
    private lateinit var lblSaludar : TextView;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar componentes del layout
        btnSaludar = findViewById(R.id.btnSaludo) as Button;
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button;
        btnCerrar = findViewById(R.id.btnCerrar) as Button;
        txtNombre = findViewById(R.id.txtSaludo) as EditText;
        lblSaludar = findViewById(R.id.lblSaludo) as TextView;


        // Metodo clic del Button: btnSaludar
        btnSaludar.setOnClickListener {
            var str : String;

            if(txtNombre.text.toString().contentEquals("")){
                Toast.makeText(
                    applicationContext,
                    "Falto capturar datos",
                    Toast.LENGTH_LONG).show();
            }
            else{
                str = "Hola " + txtNombre.text.toString() + ", ¿Como estás?";
                lblSaludar.text = str;
            }
        }


        // Metodo clic del Button: btnLimpiar
        btnLimpiar.setOnClickListener {
            txtNombre.setText("");
            lblSaludar.text = "";
        }


        // Metodo clic del Button: btnCerrar
        btnCerrar.setOnClickListener {
            finish();
        }
    }
}